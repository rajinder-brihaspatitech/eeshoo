package com.eeshoo.app.util;

public class WSConstant {

	public static class Web {
		// public static final String HOST =
		// "http://54.183.129.180/eeshoo/api/"; // development
		public static final String HOST = "http://54.187.123.220/api/"; // production
		public static final String LOGIN = "login.php";
		public static final String CATEGORIES = "categories.php";
		public static final String PRODUCT_VIEW = "product-view.php";
		public static final String PRODUCTS = "products.php";
		public static final String EXPOLORE = "home-categories.php";
		public static final String SEARCH = "search.php";
		public static final String PASSWORD = "password.php";

		public static final String DOWNLOAD = "http://54.187.123.220/fullbook/";

	}

	public static class PARAM {

		public static final String TAG_JSON_OBJECT = "jobj_req";
		public static final String EMAIL = "email";
		public static final String PWD = "password";

	}

}
