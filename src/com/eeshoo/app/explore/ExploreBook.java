package com.eeshoo.app.explore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.book.BookDetailFragment;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.model.BookDetail;
import com.eeshoo.app.parser.JsonParser;
import com.eeshoo.app.search.SearchBook;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.Web;
import com.squareup.picasso.Picasso;

public class ExploreBook extends AbstractBaseFragment implements
		OnClickListener {
	public static final String TAG = ExploreBook.class.getName();

	Button buttonLatestBook, buttonPopular, buttonTopSelling;

	public static final String KEY_ONE = "Handpicked Books";
	public static final String KEY_TWO = "Popular on Eeshoo";
	public static final String KEY_THREE = "Sale Books";
	HashMap<String, String> params;

	ImageView search;
	GridView gridview;
	ImageAdapter adapter;
	View rootView;

	private List<BookDetail> listBookDetailHandPick;
	private List<BookDetail> listBookDetailPopular;
	private List<BookDetail> listBookDetailTopSelling;
	private List<BookDetail> listBookDetails;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listBookDetails = new ArrayList<BookDetail>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_layout_explore_book,
					null);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		((BaseFragment) getParentFragment()).mActivity.tvTitle
				.setText(R.string.explore_text);

		search = ((BaseFragment) getParentFragment()).mActivity.iconSearch;
		search.setVisibility(View.VISIBLE);
		search.setOnClickListener(this);

		buttonLatestBook = (Button) rootView
				.findViewById(R.id.button_latest_book);
		buttonLatestBook.setSelected(true);
		buttonPopular = (Button) rootView
				.findViewById(R.id.button_popular_book);
		buttonTopSelling = (Button) rootView
				.findViewById(R.id.button_top_selling_book);
		gridview = (GridView) rootView.findViewById(R.id.gridview);

		addButtonClickListener(rootView);

		// search = (ImageView) rootView.findViewById(R.id.icon_search);

		adapter = new ImageAdapter(getActivity(), listBookDetails);
		gridview.setAdapter(adapter);
		if (listBookDetailHandPick == null)
			callServiceHandPick();

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setHeaderIcon(AppConstants.KEY_SEARCH);
	}

	/*
	 * Add Segmented button click listener;
	 */

	private void callServiceHandPick() {
		params = new HashMap<String, String>();
		params.put("cat_name", KEY_ONE);
		makeNetworkRequest(KEY_ONE, Web.EXPOLORE, Method.POST, params);

	}

	private void callServicePopular() {
		params = new HashMap<String, String>();
		params.put("cat_name", KEY_TWO);
		makeNetworkRequest(KEY_TWO, Web.EXPOLORE, Method.POST, params);
	}

	private void callServiceTopSelling() {
		params = new HashMap<String, String>();
		params.put("cat_name", KEY_THREE);
		makeNetworkRequest(KEY_THREE, Web.EXPOLORE, Method.POST, params);
	}

	private void updateUiElement(List<BookDetail> listBookDetails) {
		adapter.updateList(listBookDetails);

	}

	protected void parseResponse(String key, String request, String response) {
		UiWidget.hideProgressDialog();
		switch (key) {
		case KEY_ONE:
			listBookDetailHandPick = JsonParser.getCategoryProduct(response);
			updateUiElement(listBookDetailHandPick);
			break;
		case KEY_TWO:
			listBookDetailPopular = JsonParser.getCategoryProduct(response);
			updateUiElement(listBookDetailPopular);

			break;
		case KEY_THREE:
			listBookDetailTopSelling = JsonParser.getCategoryProduct(response);
			updateUiElement(listBookDetailTopSelling);

			break;

		default:
			break;
		}

	}

	private void addButtonClickListener(View rootView) {

		buttonLatestBook.setOnClickListener(new SegmentButtonClickListener(
				rootView));
		buttonPopular.setOnClickListener(new SegmentButtonClickListener(
				rootView));
		buttonTopSelling.setOnClickListener(new SegmentButtonClickListener(
				rootView));

	}

	// ImageAdapter for Gridview
	public class ImageAdapter extends BaseAdapter {
		private Context mContext;
		private Activity activity;
		private LayoutInflater inflater;
		private List<BookDetail> listBookDetails;

		public ImageAdapter(Context c, List<BookDetail> listBookDetails) {
			mContext = c;
			this.listBookDetails = listBookDetails;
		}

		public void updateList(List<BookDetail> listBookDetails) {
			this.listBookDetails = listBookDetails;
			notifyDataSetChanged();
		}

		public int getCount() {
			return listBookDetails.size();
		}

		public Object getItem(int position) {
			return listBookDetails.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		// create a new ImageView for each item referenced by the Adapter
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;
			if (inflater == null)
				inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_grid_row_image,
						null);
				holder = new ViewHolder();
				// imageView = (ImageView) convertView
				// .findViewById(R.id.image_category);
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.grid_image_row);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final BookDetail bookDetail = listBookDetails.get(position);
			Picasso.with(getActivity()).load(bookDetail.getImage())
					.into(holder.imageView);
			holder.imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String productId = bookDetail.getProductId();
					BookDetailFragment bookFragment = BookDetailFragment
							.create(productId, AppConstants.BUY);
					String tag = ((BaseFragment) getParentFragment()).mActivity.mTabHost
							.getCurrentTabTag();
					Log.i(TAG, "Current Tab:" + tag);
					((BaseFragment) getParentFragment()).pushFragments(tag,
							bookFragment, true, true);

				}
			});
			return convertView;

		}

	}

	static class ViewHolder {
		ImageView imageView;

	}

	/*
	 * All click listener below this
	 */

	public class SegmentButtonClickListener implements OnClickListener {

		View view;

		public SegmentButtonClickListener(View view) {
			this.view = view;
		}

		@Override
		public void onClick(View v) {

			int id = v.getId();
			if (id == R.id.button_latest_book) {
				if (!view.findViewById(id).isSelected()) {
					view.findViewById(R.id.button_popular_book).setSelected(
							false);
					view.findViewById(R.id.button_top_selling_book)
							.setSelected(false);
					view.findViewById(id).setSelected(true);
					if (listBookDetailHandPick == null
							|| listBookDetailHandPick.size() == 0) {
						updateUiElement(listBookDetails);
						callServiceHandPick();
					} else {
						updateUiElement(listBookDetailHandPick);
					}
				}
			} else if (id == R.id.button_popular_book) {
				if (!view.findViewById(id).isSelected()) {
					view.findViewById(R.id.button_latest_book).setSelected(
							false);
					view.findViewById(R.id.button_top_selling_book)
							.setSelected(false);
					view.findViewById(id).setSelected(true);
					if (listBookDetailPopular == null
							|| listBookDetailPopular.size() == 0) {
						updateUiElement(listBookDetails);
						callServicePopular();
					} else {
						updateUiElement(listBookDetailPopular);
					}
				}
			} else if (id == R.id.button_top_selling_book) {
				if (!view.findViewById(id).isSelected()) {
					view.findViewById(R.id.button_popular_book).setSelected(
							false);
					view.findViewById(R.id.button_latest_book).setSelected(
							false);
					view.findViewById(id).setSelected(true);
					if (listBookDetailTopSelling == null
							|| listBookDetailTopSelling.size() == 0) {
						updateUiElement(listBookDetails);
						callServiceTopSelling();
					} else {
						updateUiElement(listBookDetailPopular);
					}
				}
			}

		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		switch (id) {
		case R.id.icon_search:
			SearchBook searchBook = new SearchBook();
			String tag = ((BaseFragment) getParentFragment()).mActivity.mTabHost
					.getCurrentTabTag();
			Log.i(TAG, "Current Tab:" + tag);
			((BaseFragment) getParentFragment()).pushFragments(tag, searchBook,
					true, true);
			break;

		default:
			break;
		}

	}

	/**
	 * all network request using this method
	 * 
	 * @param request
	 *            url
	 * @param method
	 *            get/post
	 * @param rParams
	 *            request params
	 * 
	 */

	private void makeNetworkRequest(final String key, final String request,
			int method, final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + request;
		UiWidget.showProgressDialog(getActivity());

		StringRequest strReq = new StringRequest(method, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d(TAG + "On Response", response);
						parseResponse(key, request, response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

}
