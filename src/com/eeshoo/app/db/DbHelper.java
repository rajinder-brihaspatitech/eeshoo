package com.eeshoo.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.eeshoo.app.db.DbConstant.Book;
import com.eeshoo.app.db.DbConstant.User;

public class DbHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "eesho.db";
	private static final int DATABASE_VERSION = 1;

	private static final String TEXT_TYPE = " TEXT";
	private static final String INT_TYPE = " INT";
	private static final String REAL_TYPE = " REAL";
	private static final String COMMA_SEP = ",";

	// CREATE SQL STATEMENT
	private static final String CREATE_USER_TABLE = "CREATE TABLE "
			+ User.USER_TABLE + "(" + User._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + User.USER_FRIST_NAME
			+ TEXT_TYPE + COMMA_SEP + User.USER_LAST_NAME + TEXT_TYPE
			+ COMMA_SEP + User.EMAIL + TEXT_TYPE + COMMA_SEP + User.PASSWORD
			+ TEXT_TYPE + ");";
	private static final String CREATE_USER_BOOK_TABLE = "CREATE TABLE "
			+ Book.TABLE + "(" + Book._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + Book.PRODUCT_ID
			+ TEXT_TYPE + COMMA_SEP + Book.NAME + TEXT_TYPE + COMMA_SEP
			+ Book.AUTHOR + TEXT_TYPE + COMMA_SEP + Book.DESC + TEXT_TYPE
			+ COMMA_SEP + Book.IMAGE + TEXT_TYPE + COMMA_SEP + Book.USER_ID
			+ TEXT_TYPE + COMMA_SEP + Book.PRICE + TEXT_TYPE + COMMA_SEP
			+ Book.ISBN + TEXT_TYPE + COMMA_SEP + Book.PUBLICATION + TEXT_TYPE
			+ COMMA_SEP + Book.PATH + TEXT_TYPE + COMMA_SEP
			+ Book.RESOURCE_PATH + TEXT_TYPE + COMMA_SEP + Book.IS_READING
			+ INT_TYPE + ");" + "";

	private static final String SQL_DELETE_USER_TABLE = "DROP TABLE IF EXISTS "
			+ User.USER_TABLE;
	private static final String SQL_DELETE_BOOK_TABLE = "DROP TABLE IF EXISTS "
			+ Book.TABLE;

	private static DbHelper instance;

	public static synchronized DbHelper getHelper(Context context) {
		if (instance == null)
			instance = new DbHelper(context);
		return instance;
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_USER_TABLE);
		db.execSQL(CREATE_USER_BOOK_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_USER_TABLE);
		db.execSQL(SQL_DELETE_BOOK_TABLE);
		onCreate(db);
	}

}
