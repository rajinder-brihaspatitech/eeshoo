package com.eeshoo.app.home;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.eeshoo.app.R;
import com.eeshoo.app.util.UiWidget;

public class HomeActivity extends ActionBarActivity implements OnClickListener {

	Button buttonReadingList, buttonCurrentReading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_layout_home);

		buttonReadingList = (Button) findViewById(R.id.button_reading_list);
		buttonReadingList.setSelected(true);
		buttonCurrentReading = (Button) findViewById(R.id.button_current_reading);

		addButtonClickListener();

	}

	// Button click listener
	private void addButtonClickListener() {
		buttonReadingList.setOnClickListener(new SegmentButtonClickListener());
		buttonCurrentReading
				.setOnClickListener(new SegmentButtonClickListener());

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.button_current_reading:
			UiWidget.Toast(this, "Current Reading");
			break;
		case R.id.button_reading_list:
			UiWidget.Toast(this, "Reading List");

			break;

		default:
			break;
		}

	}

	public class SegmentButtonClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			int id = v.getId();
			if (id == R.id.button_current_reading) {
				if (!findViewById(id).isSelected()) {
					findViewById(R.id.button_reading_list).setSelected(false);
					findViewById(id).setSelected(true);
					UiWidget.Toast(HomeActivity.this, "Current Reading");
				}
			} else if (id == R.id.button_reading_list) {
				if (!findViewById(id).isSelected()) {
					findViewById(R.id.button_current_reading)
							.setSelected(false);
					findViewById(id).setSelected(true);
					UiWidget.Toast(HomeActivity.this, "Reading List");
				}
			}

		}
	}

}
