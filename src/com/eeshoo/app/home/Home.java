package com.eeshoo.app.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.book.BookDetailFragment;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.controller.BookHandler;
import com.eeshoo.app.db.dao.BookDAO;
import com.eeshoo.app.model.BookDetail;
import com.eeshoo.app.parser.JsonParser;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.ConnectionDetector;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.PARAM;
import com.eeshoo.app.util.WSConstant.Web;
import com.squareup.picasso.Picasso;

public class Home extends AbstractBaseFragment {

	public static final String TAG = Home.class.getName();

	public View rootView;

	// Connection detector class
	ConnectionDetector cd;
	static Boolean isInternetPresent = false;

	ImageView iconRefersh;

	Button buttonReadingList, buttonCurrentReading;

	LinearLayout containerEmpty, containerList;

	ListView listOfBook;

	List<BookDetail> listOfBooks;
	List<BookDetail> listReadingBookDetails;
	List<BookDetail> listCurrentReadingBooks;
	BookDAO bookDao;

	CustomListAdapter adapter;
	SharedPreferences sharedpreferences;

	String email, password, customerId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(TAG, "onCreate");
		cd = new ConnectionDetector(getActivity());
		bookDao = new BookDAO(getActivity());
		sharedpreferences = getActivity().getSharedPreferences(
				AppConstants.EESHOO, Context.MODE_PRIVATE);
		listOfBooks = new ArrayList<BookDetail>();
		listReadingBookDetails = new ArrayList<BookDetail>();
		listCurrentReadingBooks = new ArrayList<BookDetail>();

		isInternetPresent = cd.isConnectingToInternet();
		if (isInternetPresent) {
			init();
		} else {
			getBookFromTable();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_layout_home, null);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		((BaseFragment) getParentFragment()).mActivity.tvTitle
				.setText(R.string.home_text_title);

		buttonReadingList = (Button) rootView
				.findViewById(R.id.button_reading_list);
		buttonReadingList.setSelected(true);
		buttonCurrentReading = (Button) rootView
				.findViewById(R.id.button_current_reading);
		setHeaderIcon(AppConstants.KEY_REFRESH);

		addButtonClickListener(rootView);

		addRefereshButtonListener(rootView);

		containerEmpty = (LinearLayout) rootView
				.findViewById(R.id.container_empty_list);
		containerList = (LinearLayout) rootView
				.findViewById(R.id.listview_container);
		listOfBook = (ListView) rootView.findViewById(R.id.list_of_book);
		adapter = new CustomListAdapter(getActivity(), listOfBooks);
		listOfBook.setAdapter(adapter);
		addListViewlistener();
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		setHeaderIcon(AppConstants.KEY_REFRESH);
	}

	private void addRefereshButtonListener(View rootView) {

		((BaseFragment) getParentFragment()).mActivity.iconReferesh
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (isInternetPresent) {
							listCurrentReadingBooks = new ArrayList<BookDetail>();
							listReadingBookDetails = new ArrayList<BookDetail>();
							init();
						} else {
							listCurrentReadingBooks = new ArrayList<BookDetail>();
							listReadingBookDetails = new ArrayList<BookDetail>();
							getBookFromTable();
						}
						toggleSegment();
					}
				});

	}

	protected void toggleSegment() {
		buttonReadingList.setSelected(true);
		buttonCurrentReading.setSelected(false);

	}

	private void init() {
		if (sharedpreferences != null && sharedpreferences.contains("email")
				&& sharedpreferences.contains("password")
				&& sharedpreferences.contains("customer_id")) {
			email = sharedpreferences.getString("email", "");
			password = sharedpreferences.getString("password", "");
			customerId = sharedpreferences.getString("customer_id", "0");
			HashMap<String, String> params = new HashMap<String, String>();
			params.put(PARAM.EMAIL, email);
			params.put(PARAM.PWD, password);
			makeNetworkRequestForLogin(Web.LOGIN, params);

		}
	}

	private void getBookFromTable() {
		if (sharedpreferences != null && sharedpreferences.contains("email")
				&& sharedpreferences.contains("password")
				&& sharedpreferences.contains("customer_id")) {
			customerId = sharedpreferences.getString("customer_id", "0");
			new GetUserBookDetailTask().execute(customerId);

		}

	}

	protected void parseResponse(String response) {
		UiWidget.hideProgressDialog();
		listOfBooks = JsonParser.getUserPurchasedBookList(response);
		listReadingBookDetails = listOfBooks;
		updateReadingList(listReadingBookDetails);

	}

	public void updateReadingList(List<BookDetail> listReadingBookDetails) {
		if (listReadingBookDetails.size() != 0) {
			containerEmpty.setVisibility(View.GONE);
			containerList.setVisibility(View.VISIBLE);
			addListviewItem(listReadingBookDetails);
			addListViewlistener();
			BookHandler.getInstance().setListUserPurchasedBook(
					listReadingBookDetails);
		} else {
			containerEmpty.setVisibility(View.VISIBLE);
			containerList.setVisibility(View.GONE);
		}
	}

	public void updateCurrentReadingList(List<BookDetail> listReadingBookDetails) {
		if (listReadingBookDetails.size() != 0) {
			containerEmpty.setVisibility(View.GONE);
			containerList.setVisibility(View.VISIBLE);
			addListviewItem(listReadingBookDetails);
			addListViewlistener();

		} else {
			containerEmpty.setVisibility(View.VISIBLE);
			containerList.setVisibility(View.GONE);
		}
	}

	private void addListviewItem(List<BookDetail> listbookdetail) {
		this.listOfBooks = listbookdetail;
		adapter.updateList(listOfBooks);

	}

	// ListView itemclickListener
	private void addListViewlistener() {
		listOfBook.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				String productId = listOfBooks.get(position).getProductId();
				BookDetailFragment bookFragment = BookDetailFragment.create(
						productId, AppConstants.READ);
				String tag = ((BaseFragment) getParentFragment()).mActivity.mTabHost
						.getCurrentTabTag();
				// Log.e(TAG, "Current Tab:" + tag);
				((BaseFragment) getParentFragment()).pushFragments(tag,
						bookFragment, true, true);

			}

		});

	}

	// Button click listener
	private void addButtonClickListener(View view) {
		buttonReadingList.setOnClickListener(new SegmentButtonClickListener(
				view));
		buttonCurrentReading.setOnClickListener(new SegmentButtonClickListener(
				view));

	}

	public class GetUserBookDetailTask extends
			AsyncTask<String, Void, List<BookDetail>> {

		@Override
		protected List<BookDetail> doInBackground(String... params) {
			return bookDao.getAllBooks(params[0]);
		}

		@Override
		protected void onPostExecute(List<BookDetail> listBookDetails) {
			if (listBookDetails.size() != 0) {
				addListviewItem(listBookDetails);
				addListViewlistener();
				BookHandler.getInstance().setListUserPurchasedBook(
						listBookDetails);
			}

		}
	}

	public class GetCurrentReadingBookDetailTask extends
			AsyncTask<String, Void, List<BookDetail>> {

		@Override
		protected List<BookDetail> doInBackground(String... params) {
			return bookDao.getCurrentReadingBooks(params[0]);
		}

		@Override
		protected void onPostExecute(List<BookDetail> listBookDetails) {
			listCurrentReadingBooks = listBookDetails;
			updateCurrentReadingList(listCurrentReadingBooks);

		}
	}

	/*
	 * add all custom adapter here
	 */

	class CustomListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		private List<BookDetail> listbookdetail;

		public CustomListAdapter(Activity activity,
				List<BookDetail> listbookdetail) {
			this.activity = activity;
			this.listbookdetail = listbookdetail;
		}

		public void updateList(List<BookDetail> listbookdetail) {
			this.listbookdetail = listbookdetail;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {

			return listbookdetail.size();
		}

		@Override
		public Object getItem(int position) {

			return listbookdetail.get(position);
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_list_book_item,
						null);
				holder = new ViewHolder();
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.image_book);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.desc = (TextView) convertView
						.findViewById(R.id.description);
				holder.author = (TextView) convertView
						.findViewById(R.id.author);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			BookDetail bookDetail = listbookdetail.get(position);
			Picasso.with(getActivity()).load(bookDetail.getImage())
					.into(holder.imageView);
			holder.title.setText(bookDetail.getName());
			holder.desc.setText(bookDetail.getDescription());
			String authorName = bookDetail.getAuthor();
			if (authorName != null && !authorName.equals("")) {
				String mStringNew = getActivity().getResources().getString(
						R.string.new_change);
				holder.author.setText(bookDetail.getAuthor().equals("N/A") ? ""
						: String.valueOf(mStringNew + authorName));
			}

			else {
				holder.author.setVisibility(View.GONE);
			}
			return convertView;

		}
	}

	static class ViewHolder {
		ImageView imageView;
		TextView title;
		TextView desc;
		TextView author;

	}

	/*
	 * All click listener below this
	 */

	public class SegmentButtonClickListener implements OnClickListener {

		View view;

		public SegmentButtonClickListener(View view) {
			this.view = view;
		}

		@Override
		public void onClick(View v) {

			int id = v.getId();
			if (id == R.id.button_current_reading) {
				if (!view.findViewById(id).isSelected()) {
					view.findViewById(R.id.button_reading_list).setSelected(
							false);
					view.findViewById(id).setSelected(true);
					if (listCurrentReadingBooks.size() == 0) {
						new GetCurrentReadingBookDetailTask()
								.execute(customerId);
					} else {
						updateCurrentReadingList(listCurrentReadingBooks);
					}

				}
			} else if (id == R.id.button_reading_list) {
				if (!view.findViewById(id).isSelected()) {
					view.findViewById(R.id.button_current_reading).setSelected(
							false);
					view.findViewById(id).setSelected(true);
					updateReadingList(listReadingBookDetails);

				}
			}

		}
	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeNetworkRequestForLogin(String login,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + login;
		UiWidget.showProgressDialog(getActivity());

		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						// Log.e(TAG + "On Response", response);
						parseResponse(response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

}
