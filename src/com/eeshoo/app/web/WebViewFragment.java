package com.eeshoo.app.web;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;

public class WebViewFragment extends AbstractBaseFragment {

	public static final String ARG_WEB_URL = "WEB_URL";

	public String webUrl;
	public WebView myWebView;

	public static WebViewFragment create(String url) {
		WebViewFragment fragment = new WebViewFragment();
		Bundle args = new Bundle();
		args.putString(ARG_WEB_URL, url);
		fragment.setArguments(args);
		return fragment;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			webUrl = getArguments().getString(ARG_WEB_URL);
		}

	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_web_view,
				null);

		myWebView = (WebView) rootView.findViewById(R.id.mywebview);

		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.setWebViewClient(new MyWebViewClient());

		myWebView.loadUrl(webUrl);
		return rootView;
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			view.loadUrl(url);
			return true;
		}
	}

}
