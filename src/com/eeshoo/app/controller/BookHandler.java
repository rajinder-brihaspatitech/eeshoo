package com.eeshoo.app.controller;

import java.util.List;

import com.eeshoo.app.model.BookDetail;

public class BookHandler {

	public static BookHandler uInstance;

	List<BookDetail> listUserPurchasedBook;

	/* Static 'instance' method */
	public static BookHandler getInstance() {
		return uInstance;
	}

	public static void initInstance() {
		if (uInstance == null) {
			uInstance = new BookHandler();
		}
	}

	public List<BookDetail> getListUserPurchasedBook() {
		return listUserPurchasedBook;
	}

	public void setListUserPurchasedBook(List<BookDetail> listUserPurchasedBook) {
		this.listUserPurchasedBook = listUserPurchasedBook;
	}

}
