package com.eeshoo.app.profile;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.eeshoo.app.LoginActivity;
import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.book.BookDetailFragment;
import com.eeshoo.app.controller.BookHandler;
import com.eeshoo.app.model.BookDetail;
import com.eeshoo.app.util.AppConstants;
import com.squareup.picasso.Picasso;

public class UserProfile extends AbstractBaseFragment {

	private GridView gridView;
	public TextView iconLogout;
	TextView email;

	String emailValue;

	private int[] arrayImage;

	List<BookDetail> listBookDetails;
	GridView gridview;
	ImageAdapter adapter;
	SharedPreferences sharedpreferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sharedpreferences = getActivity().getSharedPreferences(
				AppConstants.EESHOO, Context.MODE_PRIVATE);
		Log.e("BookHandler.getInstance(): ", "" + BookHandler.getInstance());
		if (BookHandler.getInstance() != null) {
			listBookDetails = BookHandler.getInstance()
					.getListUserPurchasedBook();
		}
		if (listBookDetails == null) {
			listBookDetails = new ArrayList<BookDetail>();
		}
		// else {

		// listBookDetails = new ArrayList<BookDetail>();
		// }
		Log.e("listBookDetails:", "" + listBookDetails);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.fragment_layout_profile, null);
		((BaseFragment) getParentFragment()).mActivity.tvTitle
				.setText(R.string.profile_title);

		setHeaderIcon(AppConstants.KEY_LOGOUT);

		iconLogout = ((BaseFragment) getParentFragment()).mActivity.iconLogout;
		iconLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Editor editor = sharedpreferences.edit();
				editor.remove("email");
				editor.remove("password");
				editor.remove("customer_id");
				editor.commit();

				Intent i = new Intent(getActivity(), LoginActivity.class);
				// set the new task and clear flags
				startActivity(i);
				getActivity().finish();

			}
		});

		email = (TextView) rootView.findViewById(R.id.user_name);
		setUsername();

		gridview = (GridView) rootView.findViewById(R.id.gridview);
		adapter = new ImageAdapter(getActivity(), listBookDetails);
		gridview.setAdapter(adapter);

		return rootView;
	}

	private void setUsername() {
		// TODO Auto-generated method stub
		emailValue = sharedpreferences.getString("email", "");
		email.setText(emailValue);

	}

	// ImageAdapter for Gridview
	public class ImageAdapter extends BaseAdapter {
		private Context mContext;
		private Activity activity;
		private LayoutInflater inflater;
		private List<BookDetail> listBookDetails;

		public ImageAdapter(Context c, List<BookDetail> listBookDetails) {
			mContext = c;
			this.listBookDetails = listBookDetails;
		}

		public void updateList(List<BookDetail> listBookDetails) {
			this.listBookDetails = listBookDetails;
			notifyDataSetChanged();
		}

		public int getCount() {
			return listBookDetails.size();
		}

		public Object getItem(int position) {
			return listBookDetails.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		// create a new ImageView for each item referenced by the Adapter
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;
			if (inflater == null)
				inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_grid_row_image,
						null);
				holder = new ViewHolder();
				// imageView = (ImageView) convertView
				// .findViewById(R.id.image_category);
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.grid_image_row);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final BookDetail bookDetail = listBookDetails.get(position);
			Picasso.with(getActivity()).load(bookDetail.getImage())
					.into(holder.imageView);
			holder.imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String productId = bookDetail.getProductId();
					BookDetailFragment bookFragment = BookDetailFragment
							.create(productId, AppConstants.READ);
					String tag = ((BaseFragment) getParentFragment()).mActivity.mTabHost
							.getCurrentTabTag();
					((BaseFragment) getParentFragment()).pushFragments(tag,
							bookFragment, true, true);

				}
			});
			return convertView;

		}

	}

	static class ViewHolder {
		ImageView imageView;

	}
}
