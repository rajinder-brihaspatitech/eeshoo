package com.eeshoo.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.eeshoo.app.R;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.base.ExploreTabContainer;
import com.eeshoo.app.base.HomeTabContainer;
import com.eeshoo.app.base.ProfileTabContainer;
import com.eeshoo.app.book.BookCategoryFragment;
import com.eeshoo.app.book.ListCategoryFragment;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.model.BookCategory;
import com.eeshoo.app.parser.JsonParser;
import com.eeshoo.app.ui.MainLayout;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.ConnectionDetector;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.Web;
import com.squareup.picasso.Picasso;

public class AppMainTabActivity extends FragmentActivity {

	public int positionCurrentCategorySelected = -1;

	public static final String TAG = AppMainTabActivity.class.getName();

	// The MainLayout which will hold both the sliding menu and our main content
	// Main content will holds our Fragment respectively
	public MainLayout mainLayout;

	// ListView menu
	private ListView lvMenu;

	List<BookCategory> listBookCategories;

	// Menu button
	Button btMenu;

	public FragmentTabHost mTabHost;

	/* A HashMap of stacks, where we use tab identifier as keys.. */
	public HashMap<String, Stack<Fragment>> mStacks;

	/* Save current tabs identifier in this.. */

	// Title according to fragment
	public TextView tvTitle, textNoInternet, listMoreCategories;
	public ImageView iconSearch;
	public ImageView iconReferesh;
	public TextView iconLogout;
	public RelativeLayout header;
	SharedPreferences sharedpreferences;

	static Boolean isInternetPresent = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_tab);
		mainLayout = (MainLayout) findViewById(R.id.main_sliding_layout);

		sharedpreferences = getSharedPreferences(AppConstants.EESHOO,
				Context.MODE_PRIVATE);

		ConnectionDetector cd;
		cd = new ConnectionDetector(getApplicationContext());

		isInternetPresent = cd.isConnectingToInternet();
		if (isInternetPresent) {
			HashMap<String, String> params = new HashMap<String, String>();
			makeNetworkRequest(Web.CATEGORIES, Method.GET, params);
		} else {
			listBookCategories = new ArrayList<>();
			updateMenuListItem(listBookCategories);
		}

		tvTitle = (TextView) findViewById(R.id.header_title);
		iconSearch = (ImageView) findViewById(R.id.icon_search);
		iconReferesh = (ImageView) findViewById(R.id.icon_referesh);
		iconLogout = (TextView) findViewById(R.id.icon_Logout);
		header = (RelativeLayout) findViewById(R.id.header_bar);

		// Get menu button
		btMenu = (Button) findViewById(R.id.btn_header_back);
		btMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Show/hide the menu
				toggleMenu(v);
			}
		});

		mStacks = new HashMap<String, Stack<Fragment>>();
		mStacks.put(AppConstants.TAB_HOME, new Stack<Fragment>());
		mStacks.put(AppConstants.TAB_EXPLORE, new Stack<Fragment>());
		mStacks.put(AppConstants.TAB_PROFILE, new Stack<Fragment>());

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getString(R.string.TAB_HOME)),
						R.drawable.tab_home_selector), HomeTabContainer.class,
				null);
		mTabHost.addTab(
				setIndicator(
						mTabHost.newTabSpec(getString(R.string.TAB_EXPLORE)),
						R.drawable.tab_explore_selector),
				ExploreTabContainer.class, null);
		mTabHost.addTab(
				setIndicator(
						mTabHost.newTabSpec(getString(R.string.TAB_PROFILE)),
						R.drawable.tab_profile_selector),
				ProfileTabContainer.class, null);

	}

	public void toggleMenu(View v) {
		mainLayout.toggleMenu();
	}

	protected void openCategoriesFragment(View v) {
		// mTabHost.setCurrentTab(0);
		tvTitle.setText(getString(R.string.menu_bar_title_categories));
		iconSearch.setVisibility(View.GONE);
		iconLogout.setVisibility(View.GONE);
		iconReferesh.setVisibility(View.GONE);

		final ListCategoryFragment listCategory = new ListCategoryFragment();
		final String tag = mTabHost.getCurrentTabTag();
		((BaseFragment) getSupportFragmentManager().findFragmentByTag(tag))
				.popAllFragment(tag);
		Log.i(TAG, "Current Tab:" + tag);
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				((BaseFragment) getSupportFragmentManager().findFragmentByTag(
						tag)).pushMenuFragments(tag, listCategory, true, true);

			}
		});
		mainLayout.toggleMenu();

	}

	/*
	 * All Menu Actvitiy hanlde belo
	 */

	private void updateMenuListItem(List<BookCategory> listBookCategories) {

		textNoInternet = (TextView) findViewById(R.id.text_no_info);
		lvMenu = (ListView) findViewById(R.id.activity_main_menu_listview);
		listMoreCategories = (TextView) findViewById(R.id.list_more_category);
		if (listBookCategories.size() == 0) {
			textNoInternet.setVisibility(View.VISIBLE);
			lvMenu.setVisibility(View.GONE);
			listMoreCategories.setVisibility(View.GONE);
		} else {
			textNoInternet.setVisibility(View.GONE);
			lvMenu.setVisibility(View.VISIBLE);
			listMoreCategories.setVisibility(View.VISIBLE);
			// lvMenu.setAdapter(new ArrayAdapter<String>(this,
			// android.R.layout.simple_list_item_1, lvMenuItems));
			CustomListAdapter adapter = new CustomListAdapter(this,
					listBookCategories);
			lvMenu.setAdapter(adapter);
			lvMenu.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					onMenuItemClick(parent, view, position, id);
					// Toast.makeText(AppMainTabActivity.this,
					// "Position" + position, Toast.LENGTH_SHORT).show();
				}

			});
			// Get title textview
			listMoreCategories.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					openCategoriesFragment(v);

				}
			});
		}

	}

	private void onMenuItemClick(AdapterView<?> parent, View view,
			int position, long id) {
		if (positionCurrentCategorySelected != position) {
			// mTabHost.setCurrentTab(0);
			// tvTitle.setText(getString(R.string.menu_bar_title_feature));
			// if (iconSearch.isShown()) {
			// iconSearch.setVisibility(View.GONE);
			// }
			String categroryName = listBookCategories.get(position).getName();
			String categoryId = String.valueOf(listBookCategories.get(position)
					.getId());
			final BookCategoryFragment bookCategory = BookCategoryFragment
					.create(categroryName, categoryId);
			final String tag = mTabHost.getCurrentTabTag();
			((BaseFragment) getSupportFragmentManager().findFragmentByTag(tag))
					.popAllFragment(tag);
			Log.i(TAG, "Current Tab:" + tag);
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					((BaseFragment) getSupportFragmentManager()
							.findFragmentByTag(tag)).pushMenuFragments(tag,
							bookCategory, true, true);

				}
			});
			// Hide menu anyway
			positionCurrentCategorySelected = position;
			mainLayout.toggleMenu();
		} else {
			mainLayout.toggleMenu();
		}
	}

	public TabSpec setIndicator(TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
		// v.setBackgroundResource(resid);
		ImageView imageButton = (ImageView) v.findViewById(R.id.image_tabs);
		imageButton.setImageResource(resid);
		TextView text = (TextView) v.findViewById(R.id.text_tabs);
		// text.setTextColor(R.drawable.text_color_picker);
		text.setText(spec.getTag());
		return spec.setIndicator(v);
	}

	class CustomListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		List<BookCategory> lisBookCategories;

		public CustomListAdapter(Activity activity,
				List<BookCategory> lisBookCategories) {
			this.activity = activity;
			this.lisBookCategories = lisBookCategories;
		}

		@Override
		public int getCount() {

			return lisBookCategories.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			ImageView imageView;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.layout_list_row_category_item, null);
				holder = new ViewHolder();
				// imageView = (ImageView) convertView
				// .findViewById(R.id.image_category);
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.image_category);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			BookCategory bookCategory = lisBookCategories.get(position);
			// holder.imageView.setImageResource(mThumbIds[position]);
			if (bookCategory.getImage() != null)
				Picasso.with(AppMainTabActivity.this)
						.load(bookCategory.getImage()).resize(623, 161)
						.into(holder.imageView);
			return convertView;

		}

	}

	static class ViewHolder {
		ImageView imageView;

	}

	@Override
	public void onBackPressed() {

		if (mainLayout.isMenuShown()) {
			mainLayout.toggleMenu();
		} else {
			boolean isPopFragment = false;
			String currentTabTag = mTabHost.getCurrentTabTag();
			Log.e("currentTabTag:", "" + currentTabTag);

			if (currentTabTag.equals(AppConstants.TAB_HOME)) {
				isPopFragment = ((BaseFragment) getSupportFragmentManager()
						.findFragmentByTag(AppConstants.TAB_HOME))
						.popFragments(currentTabTag);
			} else if (currentTabTag.equals(AppConstants.TAB_EXPLORE)) {
				isPopFragment = ((BaseFragment) getSupportFragmentManager()
						.findFragmentByTag(AppConstants.TAB_EXPLORE))
						.popFragments(currentTabTag);
			} else if (currentTabTag.equals(AppConstants.TAB_PROFILE)) {
				isPopFragment = ((BaseFragment) getSupportFragmentManager()
						.findFragmentByTag(AppConstants.TAB_PROFILE))
						.popFragments(currentTabTag);
			}

			if (!isPopFragment) {

				if (mTabHost.getCurrentTab() == 0) {
					finish();
				} else {
					mTabHost.setCurrentTab(0);
				}
			}
		}
	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeNetworkRequest(final String request, int method,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + request;
		UiWidget.showProgressDialog(this);

		StringRequest strReq = new StringRequest(method, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d(TAG + "On Response", response);
						parseResponse(request, response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	protected void parseResponse(String request, String response) {
		UiWidget.hideProgressDialog();

		if (request.equals(Web.CATEGORIES)) {
			listBookCategories = JsonParser.getAllCategory(response);

			updateMenuListItem(listBookCategories);

		}

	}

}
