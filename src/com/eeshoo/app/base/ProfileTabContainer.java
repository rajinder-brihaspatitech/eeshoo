package com.eeshoo.app.base;

import java.util.Stack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eeshoo.app.R;
import com.eeshoo.app.profile.UserProfile;

public class ProfileTabContainer extends BaseFragment {

	private boolean mIsViewInited;
	private static final String TAG = ProfileTabContainer.class.getName();

	public ProfileTabContainer() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		return inflater.inflate(R.layout.container_framelayout, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i(TAG, "onActivityCreated");
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		} else {
			getChildFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}

	private void initView() {
		Log.i(TAG, "initView");
		Log.i(TAG, mActivity.mTabHost.getCurrentTabTag());

		String tagHome = mActivity.mTabHost.getCurrentTabTag();
		mStacks.put(tagHome, new Stack<Fragment>());
		UserProfile userProfile = new UserProfile();
		pushFragments(tagHome, userProfile, false, true);
		// replaceFragment(placeholder, false);

	}

}
