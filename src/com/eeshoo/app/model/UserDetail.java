package com.eeshoo.app.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserDetail implements Parcelable {

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;

	public UserDetail() {
		super();
	}

	private UserDetail(Parcel in) {
		super();
		this.id = in.readLong();
		this.firstName = in.readString();
		this.lastName = in.readString();
		this.email = in.readString();
		this.password = in.readString();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return firstName + " " + lastName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + getName() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + id);
		return result;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(getId());
		parcel.writeString(getFirstName());
		parcel.writeString(getLastName());
		parcel.writeString(getEmail());
		parcel.writeString(getPassword());
	}

	public static final Parcelable.Creator<UserDetail> CREATOR = new Parcelable.Creator<UserDetail>() {
		public UserDetail createFromParcel(Parcel in) {
			return new UserDetail(in);
		}

		public UserDetail[] newArray(int size) {
			return new UserDetail[size];
		}
	};

}
