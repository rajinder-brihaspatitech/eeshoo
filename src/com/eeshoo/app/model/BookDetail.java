package com.eeshoo.app.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BookDetail implements Parcelable {

	private long id;
	private String productId;
	private String name;
	private String author;
	private String description;
	private String image;
	private String userId;
	private String price;
	private String isbn;
	private String publishDate;
	private String path;
	private String resourcePath;
	private Integer isReading;

	// private UserDetail user;

	public BookDetail() {
		super();
	}

	public BookDetail(Parcel in) {
		super();
		this.id = in.readLong();
		this.productId = in.readString();
		this.name = in.readString();
		this.author = in.readString();
		this.description = in.readString();
		this.image = in.readString();
		// this.user = in.readParcelable(UserDetail.class.getClassLoader());
		this.userId = in.readString();
		this.price = in.readString();
		this.isbn = in.readString();
		this.publishDate = in.readString();
		this.path = in.readString();
		this.resourcePath = in.readString();
		this.isReading = in.readInt();

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	// public UserDetail getUser() {
	// return user;
	// }
	//
	// public void setUser(UserDetail user) {
	// this.user = user;
	// }

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public Integer getIsReading() {
		return isReading;
	}

	public void setIsReading(Integer isReading) {
		this.isReading = isReading;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", User =" + userId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + id);
		return result;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {

		parcel.writeLong(getId());
		parcel.writeString(getProductId());
		parcel.writeString(getName());
		parcel.writeString(getAuthor());
		parcel.writeString(getDescription());
		parcel.writeString(getImage());
		parcel.writeString(getUserId());
		parcel.writeString(getPrice());
		parcel.writeString(getIsbn());
		parcel.writeString(getPublishDate());
		parcel.writeString(getPath());
		parcel.writeString(getResourcePath());
		parcel.writeInt(getIsReading());
		;
		// parcel.writeParcelable(getUser(), flags);

	}

	public static final Parcelable.Creator<BookDetail> CREATOR = new Parcelable.Creator<BookDetail>() {
		public BookDetail createFromParcel(Parcel in) {
			return new BookDetail(in);
		}

		public BookDetail[] newArray(int size) {
			return new BookDetail[size];
		}
	};

}
